#!/usr/bin/env python3
# Copyright 2015-2021, Damian Johnson and The Tor Project
# See LICENSE for licensing information

"""
Simple script that checks to see if relays have fingerprints which are close
together on the hash ring. Multiple related relays with similar fingerprints
indicates an attack on hidden services.
"""
import codecs
import os
import time
import traceback

import util

from stem.descriptor.remote import DescriptorDownloader
from stem.util import conf


EMAIL_SUBJECT = 'Relays with nearby positions on the hash ring'

EMAIL_BODY = """\
The following relays have fingerprints which are neighbors on the hash ring...

"""
ONE_DAY = 24 * 60 * 60
DISTANCE_CUTOFF = 4
MIN_SIZE_OF_SETS = 3

log = util.get_logger('fingerprint_neighbours')

def fingerprint_distance(fingerprint, second_fingerprint):
  assert len(fingerprint) == len(second_fingerprint)
  for i in range(len(fingerprint)):
    if fingerprint[i] != second_fingerprint[i]:
      return i
  else:
    return len(fingerprint)

def contains_fingerprints(new_fingerprints, fingerprint_sets):
  for fingeprint_set in fingerprint_sets:
    if new_fingerprints.issubset(fingeprint_set):
      return True
  return False

# import pdb; pdb.set_trace()

def main():
  last_notified_config = conf.get_config('last_notified')
  last_notified_path = util.get_path('data', 'relay_keys_last_notified.cfg')

  if os.path.exists(last_notified_path):
    last_notified_config.load(last_notified_path)
  else:
    last_notified_config._path = last_notified_path

  downloader = DescriptorDownloader(timeout = 15)
  alarm_for = set()

  consensus = downloader.get_consensus()
  relay_data = {relay.fingerprint: relay for relay in consensus}
  fingerprint_list = [codecs.decode(relay.fingerprint, "hex") for relay in consensus]
  fingerprint_sets = []

  for i in range(len(fingerprint_list)-1):
    current_fp = fingerprint_list[i]
    nearby_fingerprints = []

    for index in range(i+1, len(fingerprint_list)):
      distance = fingerprint_distance(codecs.encode(current_fp, "hex").decode("ascii"),
                 codecs.encode(fingerprint_list[index], "hex").decode("ascii"))
      if distance >= DISTANCE_CUTOFF:
        nearby_fingerprints.append(fingerprint_list[index])
      else:
        break

    if nearby_fingerprints:
      new_set = set(nearby_fingerprints + [current_fp])
      if not contains_fingerprints(new_set, fingerprint_sets):
        fingerprint_sets.append(new_set)

  for fingerprint_set in fingerprint_sets:
    if len(fingerprint_set) >= MIN_SIZE_OF_SETS:
      alarm_for.add(frozenset(fingerprint_set))

  if alarm_for and not is_notification_suppressed(alarm_for):
    log.debug("Sending a notification for %i relay sets..." % len(alarm_for))
    body = EMAIL_BODY

    for fingerprint_set in alarm_for:
      for fingerprint_bytes in sorted(fingerprint_set):
        fingerprint = codecs.encode(fingerprint_bytes, "hex").decode("ascii").upper()
        body += "* {} {} {} {} {}\n".format(fingerprint,
                                      relay_data[fingerprint].address,
                                      relay_data[fingerprint].nickname,
                                      relay_data[fingerprint].or_port,
                                      relay_data[fingerprint].dir_port,
        )
      body += "\n"

    #util.send(EMAIL_SUBJECT, body = body)
    log.debug(body)

    # register that we've notified for these

    current_time = str(int(time.time()))

    for fingerprint_set in alarm_for:
      last_notified_config.set(hash(fingerprint_set), current_time)

    last_notified_config.save()


def is_notification_suppressed(fingerprint_sets):
  """
  Check to see if we've already notified for all these endpoints today. No
  point in causing too much noise.
  """

  is_all_suppressed = True
  log.debug("Checking if notification should be suppressed...")
  last_notified_config = conf.get_config('last_notified')

  for fingerprint_set in fingerprint_sets:
    key = str(hash(fingerprint_set))
    suppression_time = ONE_DAY - (int(time.time()) - last_notified_config.get(key, 0))

    if suppression_time < 0:
      log.debug("* notification for %s isn't suppressed" % key)
      is_all_suppressed = False
    else:
      log.debug("* we already notified for %s recently, suppressed for %i hours" % (key, suppression_time / 3600))

  return is_all_suppressed


if __name__ == '__main__':
  try:
    main()
  except:
    msg = "fingerprint_neighbours.py failed with:\n\n%s" % traceback.format_exc()
    log.error(msg)
    util.send("Script Error", body = msg, to = [util.ERROR_ADDRESS])
