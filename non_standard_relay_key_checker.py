#!/usr/bin/env python3
# Copyright 2015-2021, Damian Johnson and The Tor Project
# See LICENSE for licensing information

"""
Simple script that checks to see if relays are running with non-standard
relay keys. This can indicate that the operators generated keys to position
themselves in the HSDir ring. This can indicate malicious intent toward hidden services.
"""
import os
import time
import traceback

import util

from stem.descriptor.remote import DescriptorDownloader
from stem.util import conf
from Cryptodome.PublicKey import RSA


EMAIL_SUBJECT = 'Relays with non-standard identity keys'

EMAIL_BODY = """\
The following relays are running with non-standard relays keys...

"""
BAD_KEYS_FILE = util.get_path('data', 'bad_keys')
ONE_DAY = 24 * 60 * 60
TEN_DAYS = 10 * 24 * 60 * 60

log = util.get_logger('non_standard_relay_keys')


def main():
  last_notified_config = conf.get_config('last_notified')
  last_notified_path = util.get_path('data', 'relay_keys_last_notified.cfg')

  if os.path.exists(last_notified_path):
    last_notified_config.load(last_notified_path)
  else:
    last_notified_config._path = last_notified_path

  downloader = DescriptorDownloader(timeout = 15)
  alarm_for = set()

  for relay in downloader.get_server_descriptors():
    public_key = RSA.importKey(relay.signing_key)

    if public_key.e != 65537:
      log.debug("Relay key %s had non-standard public exponent %d." % (relay.fingerprint, public_key.e))
      alarm_for.add((relay.fingerprint, relay.address, public_key.e))

  if alarm_for and not is_notification_suppressed(alarm_for):
    log.debug("Sending a notification for %i relays..." % len(alarm_for))
    body = EMAIL_BODY

    for fingerprint, address, exponent in alarm_for:
      body += "* %s %s %d\n" % (fingerprint, address, exponent)

    util.send(EMAIL_SUBJECT, body = body)

    # register that we've notified for these

    current_time = str(int(time.time()))

    for fingerprint, _, _ in alarm_for:
      last_notified_config.set(fingerprint, current_time)

    last_notified_config.save()


def is_notification_suppressed(bad_keys):
  """
  Check to see if we've already notified for all these endpoints today. No
  point in causing too much noise.
  """

  is_all_suppressed = True
  log.debug("Checking if notification should be suppressed...")
  last_notified_config = conf.get_config('last_notified')

  for fingerprint, address, exponent in bad_keys:
    key = fingerprint
    suppression_time = ONE_DAY - (int(time.time()) - last_notified_config.get(key, 0))

    if suppression_time < 0:
      log.debug("* notification for %s isn't suppressed" % key)
      is_all_suppressed = False
    else:
      log.debug("* we already notified for %s recently, suppressed for %i hours" % (key, suppression_time / 3600))

  return is_all_suppressed


if __name__ == '__main__':
  try:
    main()
  except:
    msg = "non_standard_relay_key_checker.py failed with:\n\n%s" % traceback.format_exc()
    log.error(msg)
    util.send("Script Error", body = msg, to = [util.ERROR_ADDRESS])
